import Vue from "vue";
import VueRouter from "vue-router";

import Blogs from "../views/Blogs.vue";
import Post from "../views/Post.vue";
import Create from "../views/Create.vue";
import Edit from "../views/Edit.vue";
import PreviewSelectedText from "../components/PreviewSelectedText.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Blogs",
    component: Blogs,
  },
  {
    path: "/post/:id",
    name: "Post",
    component: Post,
  },
  {
    path: "/create",
    name: "Create",
    component: Create,
  },
  {
    path: "/edit/:id",
    name: "Edit",
    component: Edit,
  },
  {
    path: "/previewSelectedText",
    name: "PreviewSelectedText",
    component: PreviewSelectedText,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
