import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    posts: [
      {
        id: 1,
        title:
          "It's International Women's Day, and Quantum has a long way to go",
        author: "John Doe",
        content:
          "It's getting tiring, being a woman in tech. And @today,  on international #women's day, I want to explain why Things are tiring, but these are issues we can fix. As well as hiring more women, organizations generally need to foster an environment where women feel as empowered to succeed as their male colleagues. These changes can be as simple as thoughtfully restructuring hiring panels and recognizing the value of work that goes above and beyond to advocate for underrepresented minorities in the workplace, rather than treating these activities as extracurriculars. As this data comes from the US census it can't be generalised globally, but I decided to highlight this source in particular because the US tech industry is one of the largest and most influential, and also because it is incredibly hard to find this kind of data on a global scale from a reliable source.",
      },
      {
        id: 2,
        title:
          "The Secret of Getting Good Is Not Focusing on How Good I'm Getting",
        author: "Jane Smith",
        content:
          "I've started noticing the astonishing extent to which I'm busy worrying about how well I am doing instead of focusing on the task at hand. Not only is it perpetually exhausting, but it prevents me from giving my full attention to what I'm actually doing. This, of course, impedes my progress, further rubbing my face in the lack of achievement that I'm already panicked about which, in turn, gives me even less focus. Lately, my therapy has involved learning to tolerate my feelings enough to explore what's actually going on inside me. One of the things I found as I poked around in there is that I am constantly measuring myself. I've always done this, but never realized how relentless and distracting it was. Having already spent decades discrediting my false narratives of worthlessness, I am now able to see the feelings that spawned them for the childhood vestige they are. Without continuously getting sucked down into the rabbit hole of self-doubt, I am better able to simply watch where my attention is and isn't, and it is naturally shifting to where I want it to be! Finally, I'm finding access to the world of focus, immersion, discovery, experience, and fun waiting to be ripped open and enjoyed.",
      },
      {
        id: 3,
        title: "Become a verified book author",
        author: "Bob Johnson",
        content:
          "Medium is different from other places where writing happens, and we've designed author verification to work differently from how verification happens on other platforms. These differences are based on close feedback loops from a diverse range of authors and Medium users that we worked with while developing this feature (read more on their thoughts below). Here are answers to some questions we anticipate you might have right away We worked with book authors on Medium along the way, and they gave us lots of helpful feedback as we were building this. Here are some of their thoughts on how publishing books connects to their writing on Medium Will you open up verification to more categories over time? Maybe! We’re starting with book authors to introduce the concept of verification on Medium, and to invite the community to share their thoughts about verification in general. We welcome any and all constructive feedback here, along with ideas for where we might take verification on Medium next.",
      },
    ],
    PreViewSelectedText: [],
  },
  mutations: {
    addPost(state, post) {
      state.posts.push(post);
    },
    updatePost(state, updatedPost) {
      const postIndex = state.posts.findIndex(
        (post) => post.id === updatedPost.id
      );
      state.posts.splice(postIndex, 1, updatedPost);
    },
    deletePost(state, postId) {
      const postIndex = state.posts.findIndex((post) => post.id === postId);
      state.posts.splice(postIndex, 1);
    },
    setPreViewSelectedText(state, selectedText) {
      state.PreViewSelectedText.push(selectedText);
    },
  },
  actions: {
    addPost({ commit }, post) {
      commit("addPost", post);
    },
    updatePost({ commit }, updatedPost) {
      commit("updatePost", updatedPost);
    },
    deletePost({ commit }, postId) {
      commit("deletePost", postId);
    },
    setPreViewSelectedText({ commit }, selectedText) {
      commit("setPreViewSelectedText", selectedText);
    },
  },
  getters: {
    getPostById: (state) => (id) => {
      const post = state.posts.find((post) => post.id === id);
      return post || {};
    },
    getExcerpt: () => (post) => {
      return post.content.slice(0, 100) + "...";
    },
  },
});
